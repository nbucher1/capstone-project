﻿using UnityEngine;
using System.Collections;
using GSC;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public delegate bool MyBoolDelegate(ButtonType button);



public class Menu : MonoBehaviour {
    GameSceneController gsc;
    //Sprite bgSprite;
    public GameObject buttonObjContainer;
    public GameObject textObjContainer;
    public GameObject scrollableTextObjContainer;

    Vector3 nextItemCoords;
    public bool shouldPause = false;
    public List<MyBoolDelegate> eventListenerConditions;
    public  List<ButtonType> eventListenerParams;
    public List<MyDelegate> eventListenerActions;


    // Use this for initialization
    public void Start()
    {
        gsc = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();
        nextItemCoords = new Vector3(0, 0, 0);
        eventListenerConditions = new List<MyBoolDelegate>();
        eventListenerParams = new List<ButtonType>();
        eventListenerActions = new List<MyDelegate>();
    }


    public void AddEventListener(MyBoolDelegate condition, ButtonType param, MyDelegate action)
    {
        StartCoroutine(AddEventListenerCoroutine(condition, param, action));
    }
    private IEnumerator AddEventListenerCoroutine(MyBoolDelegate condition, ButtonType param, MyDelegate action)
    {
        yield return new WaitForEndOfFrame();
        eventListenerConditions.Add(condition);
        eventListenerParams.Add(param);
        eventListenerActions.Add(action);
    }
    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < eventListenerConditions.Count; i++)
        {
            if (eventListenerConditions.ToArray()[i](eventListenerParams.ToArray()[i]))
            {
                eventListenerActions.ToArray()[i]();
            }
        }
    }
    


    /// <summary>
    /// Creates a new text area on this menu.
    /// </summary>
    /// <param name="buttonText"></param>
    /// <param name="onClick"></param>
    public void AddText(string text, int fontSize = 24, Color? color = null, FontStyle fontStyle = FontStyle.Normal)
    {
        //instantiate the text object
        var tObj = Instantiate(textObjContainer, transform.position + nextItemCoords, transform.rotation) as GameObject;
        //parent the button to the menu
        var tTrans = tObj.GetComponent<RectTransform>();
        tTrans.SetParent(this.transform);
        //TODO: make the menu have it's own independent width and height 
        //tTrans.sizeDelta = new Vector2(3 * buttonObj.GetComponent<RectTransform>().sizeDelta.x, tTrans.sizeDelta.y);
        //set the text
        var t = tObj.GetComponentInChildren<Text>();
        t.text = text;
        t.fontSize = fontSize;
        t.color = color.GetValueOrDefault(Color.black); //color default
        t.fontStyle = fontStyle;
        //get the position of the next menu item
        nextItemCoords.y -= buttonObjContainer.GetComponent<RectTransform>().sizeDelta.y - 5.0f;//(t.preferredHeight);
    }
    
        /// <summary>
    /// Creates a new scrollable text area on this menu.
    /// </summary>
    /// <param name="text"></param>
    public void AddTextScrollable(string text)
    {
        //instantiate the text area
        var tObj = Instantiate(scrollableTextObjContainer, transform.position + nextItemCoords, transform.rotation) as GameObject;
        //parent the text area to the menu
        var tTrans = tObj.GetComponent<RectTransform>();
        tTrans.SetParent(this.transform);
        tTrans.SetSiblingIndex(1);
        //set the width to be the width of the button prefab
        //TODO: make the menu have it's own independent width and height 
        // tTrans.sizeDelta = new Vector2(3 * buttonObj.GetComponent<RectTransform>().sizeDelta.x, tTrans.sizeDelta.y);
        //set the text
        var t = tObj.GetComponentInChildren<Text>();
        t.text = text;
        //set the scrollbar's value
        var s = tObj.GetComponentInChildren<Scrollbar>();
        StartCoroutine(SetScrollVal(1, s));
        //get the position of the next menu item
        var textMaskSize = tTrans.GetComponentInChildren<Mask>().gameObject.GetComponent<RectTransform>().sizeDelta;
        //nextItemCoords.y -= textMaskSize.y;
        nextItemCoords.y -= buttonObjContainer.GetComponent<RectTransform>().sizeDelta.y;
    }

    private IEnumerator SetScrollVal(float val, Scrollbar s)
    {
        yield return new WaitForEndOfFrame();
        s.value = val;
    }


    /// <summary>
    /// Creates a new button on this menu with text, buttonText, that calls onClick when pressed.
    /// </summary>
    /// <param name="buttonText"></param>
    /// <param name="onClick"></param>
    public void AddButton(string buttonText, MyDelegate onClick)
    {
        //instantiate the button
        var bContainerObj = Instantiate(buttonObjContainer, transform.position + nextItemCoords, transform.rotation) as GameObject;
        //parent the button to the menu
        var bContainerTrans = bContainerObj.GetComponent<RectTransform>();
        bContainerTrans.SetParent(this.transform);
        //add the onclick function
        var bObj = bContainerObj.transform.FindChild("ButtonObj");
        var b = bObj.GetComponent<Button>();
        b.onClick.AddListener(() => { onClick(); });
        //set the text
        bObj.GetChild(0).GetComponent<Text>().text = buttonText;
        //get the position of the next button
        var bTrans = bObj.GetComponent<RectTransform>();
        nextItemCoords.y -= 2 * bTrans.sizeDelta.y - 20;
    }
}
