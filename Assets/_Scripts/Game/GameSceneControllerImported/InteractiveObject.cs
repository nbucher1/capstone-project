﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

namespace GSC
{
    public abstract class InteractiveObject : GameElement
    {
        //controls the hoverable and interactable(dialogue ect) aspects of the interactive objects
        [HideInInspector]
        public bool canInteract = true;
        protected Light interactiveLight;
        protected bool isInteracting;
        [HideInInspector]
        public int interactionCount;
        protected ParticleSystem hoverEffect;
        protected ParticleSystem interactiveEffect;

        [HideInInspector]
        public string objID;
        //private GameObject itemNameObject;


        protected override void Initialize()
        {
            base.Initialize();
            //TODO: Remove this
            PlayerPrefs.DeleteAll();//DELETES ALL PLAYERPREFS!!!!!
            if (gsc.debugMode) PlayerPrefs.DeleteAll(); //if we are in debug mode
            //generates a (probably very unique) id
            objID = transform.position.x + "_" + transform.position.y + "_" + transform.position.z + "_" +
                    transform.rotation.x + "_" + transform.rotation.y + "_" + transform.rotation.z;
            //            Debug.Log("objID " + elementID);

            if (GetComponentsInChildren<ParticleSystem>() != null)
            {
                hoverEffect = GetComponentsInChildren<ParticleSystem>().Where(p => p.name == "HoverEffect").SingleOrDefault();
                if(hoverEffect != null)
                {
                    Dehover(); //make sure the hover effect doesn't play at the start
                    hoverEffect.Clear();
                }
                //the interactive particle systems and light should be active whenever this item can be interacted with
                interactiveEffect = GetComponentsInChildren<ParticleSystem>().Where(p => p.name == "InteractiveEffect").SingleOrDefault();
                if (interactiveEffect != null)
                {
                    interactiveEffect.Clear();
                }
                StopInteracting();
            }
            interactionCount = -1;
            isInteracting = false;
        }



        public void Hover()
        {
            if (hoverEffect == null) return;
            //Debug.Log("Selected");
            hoverEffect.Play();
            //TODO: create the text over the item
            //itemNameObject = Instantiate(new GameObject(), Vector3.zero, Quaternion.identity) as GameObject;
            //itemNameObject.AddComponent<TextMesh>();                       //add the text component
            //itemNameObject.GetComponent<TextMesh>().font = gameSceneController.menuUtility.dialogueFont;      //set the font
            //itemNameObject.GetComponent<TextMesh>().text = itemName;       //add the message segment to the dialogue
            //itemNameObject.GetComponent<TextMesh>().color = new Color(255, 255, 255, 190);  //alpha needs to be a tiny bit larger than 0 to be able to be faded
            //itemNameObject.GetComponent<TextMesh>().fontSize = 30;
            //itemNameObject.GetComponent<TextMesh>().anchor = TextAnchor.UpperCenter;
            //itemNameObject.transform.localScale = new Vector3(-.25f, .25f, .25f);
            //var textPosition = transform.position + new Vector3(0, 3, 0);
            //itemNameObject.transform.localPosition = textPosition;  //set the position   
        }



        public void Dehover()
        {
            if (hoverEffect == null) return;
            //Debug.Log("Deselected");
            hoverEffect.Stop();
            //hoverEffect.Clear();
            //Destroy(itemNameObject);
        }

        
        /// <summary>
        /// Interact will need to be overridden in subclasses for object specific interactions. This function must end with a call to StopInteracting.
        /// </summary>
        public void Interact()
        {
            interactionCount++;
            StartInteracting();
            OnInteract();
            StopInteracting();
        }

        public abstract void OnInteract();

        private void StartInteracting()
        {
            if (interactiveEffect != null)
            {
                interactiveEffect.Play();
            }
            //isInteracting = true;
        }
        private void StopInteracting()
        {
            if(interactiveEffect != null)
            {
                interactiveEffect.Stop();
            }
            isInteracting = false;
        }
        public bool CanInteract()
        {
            if (!isInteracting && canInteract) return true;
            else return false;
        }


        public void DisableObjectThenStopInteracting()
        {
            Dehover();
            //hoverEffect.Stop();
            //hoverEffect.Clear();
            hoverEffect.gameObject.SetActive(false);
            interactiveEffect.Stop();
            interactiveEffect.Clear();
            interactiveEffect.gameObject.SetActive(false);
            StopInteracting();
        }
    }
}
