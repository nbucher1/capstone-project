﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

namespace GSC
{
    public class GameSceneController : MonoBehaviour
    {
        public enum GameState
        {
            PLAYING,
            PAUSED  //maybe change to SUSPENDED because this doesn't change Time.timeScale and only affects player input in Player.Update()
        }
        [HideInInspector]
        public PopupController popupController;
        [HideInInspector]
        public DialogueController dialogueController;
        [HideInInspector]
        public MenuUtility menuUtility;
        [HideInInspector]
        public TimerUtility timerUtility;
        [HideInInspector]
        public Player player;
        [HideInInspector]
        public Transition transition;
        [HideInInspector]
        public GameState curGameState;
        [HideInInspector]
        public List<EnemyNPC> npcs;
        [HideInInspector]
        public List<InteractiveObject> interactiveObjects;
        [HideInInspector]
        public MyCamera myCamera;
        private GameObject pauseText;
        [HideInInspector]
        public GameObject canvas;
        [HideInInspector]
        public int numTimesLoaded;
        [HideInInspector]
        public string lastSceneLoaded;
        [HideInInspector]
        public Bounds topSpawnerBounds;
        [HideInInspector]
        public AudioSource soundPlayer;
        [HideInInspector]
        public Text scoreText;
        [HideInInspector]
        public Joystick joystick;


        public bool debugMode = true;



        protected void Initialize()
        {

            var joystickObj = GameObject.Find("Joystick");
            if (joystickObj != null) joystick = joystickObj.GetComponent<Joystick>();
            

            var soundPlayerObj = GameObject.Find("SoundPlayer");
            if (soundPlayerObj != null) soundPlayer = soundPlayerObj.GetComponent<AudioSource>();

            var topSpawnerObj = GameObject.Find("TopSpawner");
            if(topSpawnerObj!= null) topSpawnerBounds = topSpawnerObj.GetComponent<BoxCollider2D>().bounds;

            //hides the cursor and locks it to the center of the screen
            //Cursor.visible = false;
            //Cursor.lockState = CursorLockMode.Locked;
            //get the name of the last scene which was loaded
            lastSceneLoaded = PlayerPrefs.GetString("LastSceneLoaded");
            if(lastSceneLoaded == null) lastSceneLoaded = "";
            //Debug.Log("Last scene loaded: '" + lastSceneLoaded + "'");

            //count the number of times this scene has been loaded
            numTimesLoaded = PlayerPrefs.GetInt("Loaded " + SceneManager.GetActiveScene().name, 0);
            //if we are starting from the editor, set lastSceneLoaded to 0
            if (lastSceneLoaded == SceneManager.GetActiveScene().name && debugMode == true) numTimesLoaded = 0;//<<<<<<<<<<<<<FOR DEBUG PURPOSES
            PlayerPrefs.SetInt("Loaded " + SceneManager.GetActiveScene().name, numTimesLoaded + 1);

            canvas = GameObject.Find("Canvas");

            var myCameraObj = GameObject.Find("Main Camera");
            if(myCameraObj != null) myCamera = myCameraObj.GetComponent<MyCamera>();
            else Debug.Log("Couldn't find myCameraObj in the GameSceneController");

            var dialogueControllerObj = GameObject.Find("DialogueController");
            if(dialogueControllerObj != null) dialogueController = dialogueControllerObj.GetComponent<DialogueController>();
            else Debug.Log("Couldn't find dialogueControllerObj in the GameSceneController");

            var popupControllerObj = GameObject.Find("PopupController");
            if(popupControllerObj != null) popupController = popupControllerObj.GetComponent<PopupController>();
            else Debug.Log("Couldn't find popupControllerObj in the GameSceneController");

            var utilityManagerObj = GameObject.Find("UtilityManager");
            if(utilityManagerObj != null) 
            {
                menuUtility = utilityManagerObj.GetComponent<MenuUtility>();
                timerUtility = utilityManagerObj.GetComponent<TimerUtility>();
            }
            else Debug.Log("Couldn't find utilityManagerObj in the GameSceneController");

            ///*
            var playerObj = GameObject.Find("Player");
            if(playerObj != null) player = playerObj.GetComponent<Player>();
            else Debug.Log("Couldn't find playerObj in the GameSceneController");
            //*/
            var transitionObj = GameObject.Find("Transition");
            if(transitionObj != null) transition = transitionObj.GetComponent<Transition>();
            else Debug.Log("Couldn't find playerObj in the GameSceneController");
            
            var rawNPCs = GameObject.FindObjectsOfType<EnemyNPC>();              //get the npcs from the scene
            for (int i = 0; i < rawNPCs.Length; i++) npcs.Add(rawNPCs[i]);  //add the npcs from the scene to the npc list
            if (rawNPCs.Length != npcs.Count) Debug.LogError("Error adding the npcs from the scene into the npc list in the GameSceneController");
            
            var rawInteractiveObjects = GameObject.FindObjectsOfType<InteractiveObject>();
            for (int i = 0; i < rawInteractiveObjects.Length; i++) interactiveObjects.Add(rawInteractiveObjects[i]);  //add the items from the scene to the item list
            if (rawInteractiveObjects.Length != interactiveObjects.Count) Debug.LogError("Error adding the items from the scene into the item list in the GameSceneController");




            var scoreTextObj = GameObject.Find("Score");
            if(scoreTextObj != null) scoreText = scoreTextObj.GetComponent<Text>();


            timerUtility.UnlockControls();      //start each scene with the controls defaulted to unlocked
            curGameState = GameState.PLAYING;   //start each scene with the curGameState defaulted to PLAYING





        }


        /// <summary>
        /// Returns the distance between the positions of two GameObjects
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static float DistanceBetween(GameObject a, GameObject b)
        {
            return Mathf.Abs((a.transform.position - b.transform.position).magnitude);
        }

        public bool IsPlaying()
        {
            return curGameState == GameState.PLAYING;
        }

        public bool IsPaused()
        {
            return curGameState == GameState.PAUSED;
        }



        /// <summary>
        /// Returns the NPC object which has the name, creatureName
        /// </summary>
        /// <param name="creatureName"></param>
        /// <returns></returns>
        public EnemyNPC getNPC(string creatureName)
        {
            if (npcs.Count == 0)
            {
                Debug.LogError("Trying to get an npc when there are no npcs in the scene");
                return null;
            }
            return npcs.Where(c => c.creatureName == creatureName).Single();
        }



        void FixedUpdate()
        {
            if(DynamicInput.IsPressed_P1(ButtonType.START))
            {
                //register pausing here, unpausing in the pauseGameCoroutine
                //if (curGameState == GameState.PLAYING) PauseGame();
            }
        }



        /// <summary>
        /// Pauses the game and shows the menu
        /// </summary>
        public void PauseGame()
        {
            curGameState = GameState.PAUSED;

            menuUtility.MoveInHierarchy(transition.gameObject, 1000);
            transition.DimScreen();

            //StartCoroutine(PauseGameCoroutine());
        }
        private IEnumerator PauseGameCoroutine()
        {
            //pause game
            Time.timeScale = 0;
            curGameState = GameState.PAUSED;
            #region instantiate the pause menu
            menuUtility.MoveInHierarchy(transition.gameObject, 1000);
            transition.DimScreen();
            pauseText = Instantiate(new GameObject(), Vector2.zero, Quaternion.identity) as GameObject;
            var pauseTextText = pauseText.AddComponent<Text>();
            pauseTextText.font = menuUtility.dialogueFont;
            pauseTextText.alignment = TextAnchor.UpperCenter;
            pauseTextText.text = "Game Paused\n\n Press start to continue";
            pauseText.transform.SetParent(GameObject.Find("Canvas").transform);
            var pauseTextRectTransform = pauseText.GetComponent<RectTransform>();
            pauseTextRectTransform.sizeDelta = new Vector2(800, 100);
            pauseTextRectTransform.anchoredPosition = new Vector3(0, 0, 0);
            #endregion
            //wait for the text to be instantiated
            yield return new WaitForEndOfFrame();
            yield return null;
            yield return 0;
            //wait for player to press start again
            while(!DynamicInput.IsPressed_P1(ButtonType.START)) yield return null;
            //resume game
            Time.timeScale = 1;
            curGameState = GameState.PLAYING;
            //destroy the pause menu
            menuUtility.MoveInHierarchy(transition.gameObject, -1000);
            transition.UndimScreen();
            Destroy(pauseText);
        }




        ///// <summary>
        ///// Pauses the game, but does not show any menus
        ///// </summary>
        //public void LockGame()
        //{
        //    //Time.timeScale = 0;
        //    player.LockPlayer();
        //    //maybe lock the other demons in the scene
        //    curGameState = GameState.LOCKED;
        //}
        ///// <summary>
        ///// Resumes the game and does not destroy any menus
        ///// </summary>
        //public void UnlockGame()
        //{
        //    //Time.timeScale = 1;
        //    player.UnlockPlayer();
        //    //maybe lock the other demons in the scene
        //    curGameState = GameState.PLAYING;
        //}

        /// <summary>
        /// Safely instantiates the creature's explosion particle system
        /// </summary>
        public void ExplodeCreature(Creature c)
        {
            //instantiate the explosion(would be better if we did object pooling, but this is fine)
            Instantiate(c.explodeObj, c.transform.position, Quaternion.identity);
        }
    }
}