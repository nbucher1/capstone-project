﻿using UnityEngine;
using System.Collections;

namespace GSC
{
    public class GameScene1Controller : GameSceneController
    {

        // Use this for initialization
        void Start()
        {
            //initializes the base GameSceneController
            base.Initialize();
            //start the bg music if it's not playing
            if(!soundPlayer.isPlaying)
            {
                soundPlayer.Play();
            }

            //timerUtility.WaitThenDo(.01f, Intro_A_01);
            
        }

        #region Intro
        public void Intro_A_01()
        {


            var choiceMenu = menuUtility.CreateEmptyMenu(0,30);
            choiceMenu.AddTextScrollable(@"Why don't you go 
to the training simulator and shoot down some fake aliens because you're a terrible shot. Really,
I wouldn't trust you with anyone's life on this space station.");
            choiceMenu.AddButton("okay", delegate ()
            {
                popupController.ShowPopup("You go to the training area");
            });
            choiceMenu.AddButton("Nope", delegate ()
            {
                Destroy(choiceMenu.gameObject);

                dialogueController.ShowDialogue(player, "Hi");
            });





            //var choiceMenu = menuUtility.CreateEmptyMenu();
            //choiceMenu.AddButton("Ask about dog", delegate ()
            //{
            //    popupController.ShowPopup("Go down the hall. He's in the lounge!");
            //});

            //GameObject.Destroy(player.gameObject);
            //enemySpawner.SpawnGalagmanauts(1);
            //timerUtility.WaitThenDo(Random.Range(1, 4), Intro_A_01);
            //popupController.ShowPopupThenDo("This is an example popup from the GameScene1Controller.cs script!", Intro_A_02);
        }
        public void Intro_A_02()
        {
            //popupController.ShowPopup("For more information on how to use the GameSceneController Package, see the comments and the readme!");
        }
        #endregion
    }
}
