﻿using UnityEngine;
using System.Collections;
using GSC;

public class Store1 : InteractiveObject
{
    Menu m;

    // Use this for initialization
    void Start()
    {
        base.Initialize();
        GetComponent<AudioSource>().Stop();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void OnInteract()
    {
        if(!isInteracting)//interactionCount % 2 == 0 && !IsInteracting())
        {
            isInteracting = true;
            GetComponent<AudioSource>().Play();
            interactiveEffect.Play();
            hoverEffect.Stop();   //but don't clear it (let the interactive effect blend into it)
                                  //gsc.popupController.ShowPopupThenDo("Welcome to my store!", Interact2);
            gsc.curGameState = GameSceneController.GameState.PAUSED;    //stop normal input
            m = gsc.menuUtility.CreateEmptyMenu(0, 100);
            m.AddTextScrollable(@"
Welcome to my store!
                
    I sell all sorts of goods. For now I'm going to talk on and on because I'm trying to test the menu system. There was a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. I told the pilot of the ship of the a time when I was at this very store and a ship came up to my storefront. And that was what happened.
                
Thanks for listening!");
            m.AddButton("OK", Interact2);
            //m.AddEventListener(DynamicInput.IsPressed_P1, ButtonType.A, Interact2);
        }
    }
    public void Interact2()
    {
        gsc.curGameState = GameSceneController.GameState.PLAYING;    //resume normal input
        //destroy the menu
        Destroy(m.gameObject);
        if (gsc.player.interactiveObject.objID == objID)
        {
            hoverEffect.Play();
        }
    }
}
