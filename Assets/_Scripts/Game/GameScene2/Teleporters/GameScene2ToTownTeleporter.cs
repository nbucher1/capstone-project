﻿using UnityEngine;
using System.Collections;
using GSC;


public class GameScene2ToTownTeleporter : InteractiveObject {

	// Use this for initialization
	void Start () {
        base.Initialize();
        Debug.Log("I should make a generic teleporter script<<<");
	}
	
	// Update is called once per frame
	void Update () {

    }


    public override void OnInteract()
    {
        interactiveEffect.Play();
        hoverEffect.Stop();   //but don't clear it (let the interactive effect blend into it)
        //check conditions to where the player needs to go here
        gsc.transition.LoadLevel("Game_Scene_01");

        //no need to stop interacting, because we're loading another level
    }
}
