﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.UI;

namespace GSC
{
    public class Player : Creature
    {
        public float speed = 25;


        public InteractiveObject interactiveObject;
        int interactiveObjectID = -1;
        BoxCollider2D col;


        // Use this for initialization
        void Start()
        {
            base.Initialize();
            col = GetComponent<BoxCollider2D>();
            interactiveObject = null;
            equippedItems = new List<EquipableItem>();
            //load the name
            creatureName = PlayerPrefs.GetString("PlayerName");
            //the player starts with twice as good stats as the base creature stats
            this.stats = this.stats + this.stats;

        }



        void Update()
        {
            //don't shoot if we're paused 
            if (gsc.IsPaused()) return;

            //if we have an interactive object and press a key
            if (interactiveObject != null && Input.anyKey)
            {
                if (DynamicInput.IsPressed_P1(ButtonType.A))
                {
                    interactiveObject.Interact();
                }
            }

            /*
            if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
            {
                //get the mouse position in world space
                var mposWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mposWorld.z = transform.position.z;
                //look at that position
                LookAtWorldCoord(mposWorld);
            }
            //*/


            var movement = Vector2.zero;
            if (Input.anyKey)
            {
                if (gsc.joystick.hasInput)
                {
                    movement = gsc.joystick.directionVector;
                }
                else
                {
                    movement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0) * 40;
                }
                //LookAtPlayerLocalCoord(movement);   //look in the direction we're going
            }
            movement = movement * (speed / 100.0f) * Time.deltaTime;
            //var hit = Physics2D.BoxCast(transform.position, 
            //    col.bounds.size, 0, movement, movement.magnitude, LayerMask.NameToLayer("Player"));
            //if(hit.collider == null)
            //{
                transform.position += (Vector3)movement; //move
            //}
            //else
            //{
            //    //movement.x = (transform.position.x - hit.point.x) + movement.x;//(transform.position.x + movement.x);
            //    //movement.y = (transform.position.y - hit.point.y) + movement.y;//(transform.position.y + movement.y);
            //    //Debug.Log("pos: " + hit.point);
            //    //transform.position += (Vector3)movement; //move
            //}
        }


        public void LookAtWorldCoord(Vector3 worldCoord)
        {
            //get the local position
            var localCoord = worldCoord - transform.position;
            if (localCoord.y < 0)
            {
                localCoord.y *= -1;
                localCoord.x *= -1;
            }
            //have the player look at the local position
            LookAtPlayerLocalCoord(localCoord);
        }

        private void LookAtPlayerLocalCoord(Vector3 localCoord)
        {
            localCoord.z = transform.position.z;
            transform.up = localCoord;
        }

        public void AddToScore(int numPoints)
        {
            SetScore(score + numPoints);
        }
        public void ResetScore()
        {
            SetScore(0);
        }
        public void SetScore(int numPoints)
        {
            score = numPoints;
            gsc.scoreText.text = "Score: " + score;
        }



        void OnTriggerEnter2D(Collider2D coll)
        {
            //if we run into an interactive object and are not currently interacting with anything
            if (coll.GetComponent<InteractiveObject>() != null && interactiveObject == null && coll.name != "Player")
            {
                //set the new interactive object as the current interactive object
                interactiveObject = coll.GetComponent<InteractiveObject>();
                //save the interactive object's id
                interactiveObjectID = coll.GetInstanceID();
                interactiveObject.Hover();
            }
        }
        
        
        
        void OnTriggerExit2D(Collider2D coll)
        {
            //if the player walks away from the current interactive object and we had an interactive object
            if (coll.GetInstanceID() == interactiveObjectID && interactiveObject != null)
            {
                interactiveObject.Dehover();
                interactiveObject = null;
                interactiveObjectID = -1;
            }
        }
        


        /// <summary>
        /// Starts a coroutine that plays the animation clip, clipName, and sets isLockedForAnimation on the player script 
        /// to true until the animation clip has completed.
        /// </summary>
        /// <param name="clipName"></param>
        /// <returns></returns>
        public void PlayAnimationClip(string clipName)
        {
            StartCoroutine(LockPlayerForAnimationClipAndPlayClip(clipName));
        }
        

        
        /// <summary>
        /// Plays the animation clip, clipName, and sets isLockedForAnimation on the player script to true until the 
        /// animation clip has completed.
        /// </summary>
        /// <param name="clipName"></param>
        /// <returns></returns>
        IEnumerator LockPlayerForAnimationClipAndPlayClip(string clipName)
        {
            isLockedForAnimationClip = true;
            yield return new WaitForEndOfFrame();
            animator.Play(clipName);
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);    //wait for animation clip to finish
            isLockedForAnimationClip = false;
        }

        
    }
}