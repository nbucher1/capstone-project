﻿using UnityEngine;
using System.Collections;

namespace GSC
{
    [RequireComponent (typeof (Camera))]
    public class MyCamera : MonoBehaviour
    {
        GameSceneController gsc;
        Vector3 oldPlayerPos;

        // Use this for initialization
        void Start()
        {
            gsc = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();
            oldPlayerPos = new Vector3(0,0,0);
        }

        void Update()
        {
            if (gsc.player == null) return;
            //var pPos = gsc.player.transform.position;
            //this.transform.position = new Vector3(pPos.x, pPos.y, transform.position.z); 
            var pPosDiff = gsc.player.transform.position - oldPlayerPos;
            var newPos = transform.position;
            if (gsc.player.transform.position.x > transform.position.x + 5)
            {
                newPos.x += pPosDiff.x;
            }
            else if (gsc.player.transform.position.x < transform.position.x - 5)
            {
                newPos.x -= -pPosDiff.x;
            }

            if (gsc.player.transform.position.y > transform.position.y + 2)
            {
                newPos.y += pPosDiff.y;
            }
            else if (gsc.player.transform.position.y < transform.position.y - 2)
            {
                newPos.y -= -pPosDiff.y;
            }
            transform.position = newPos;

            oldPlayerPos = gsc.player.transform.position;
        }
    }
}
